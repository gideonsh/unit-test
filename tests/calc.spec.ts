import { expect } from "chai";
import { sum, multiply } from "../src/calc";



describe("describe: ", () => {
    context("sum: ", () => {
        it("check that sum exist", () => {
            expect(sum).be.a("function");
        });
        it("check with 2 numbers", () => {
            expect(sum(2,3)).to.equal(5);
        });
        it("check with several numbers", () => {
            expect(sum(1,2,3,4)).to.equal(10);
        });
    });
    context("multiply: ", () => {
        it("check that multiply exist", () => {
            expect(multiply).be.a("function");
        });
        it("check with 2 numbers", () => {
            expect(multiply(2,1,2)).to.deep.equal([2,4]);
        });
        it("check with several numbers", () => {
            expect(multiply(2,1,2,3,4)).to.deep.equal([2,4,6,8]);
        });
    });
    context("async: ", () => {
        it("check async function", () => {
            setTimeout(() => {console.log("async test");});
            expect(multiply).be.a("function");
        });
    });
});


