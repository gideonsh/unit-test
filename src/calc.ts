







export function sum(...numbers : number[]) : number {
    return numbers.reduce((acc:number , num:number) => {return acc + num;});
}

export function multiply(multi : number, ...numbers : number[]) : number[] {
    return numbers.map((number:number) => {return number * multi;});
}



